<?php
class Rtl_Support extends Plugin {

	private $host;

	function about() {
		return array(1.0,
			"Set Right To Left language feed",
			"sagiben",
			false);
	}

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this);
		$host->add_hook($host::HOOK_PREFS_EDIT_FEED, $this);
		$host->add_hook($host::HOOK_PREFS_SAVE_FEED, $this);

		// Debug::set_enabled(true);
		// Debug::set_loglevel(1);
		// $this->log = Logger::get();
	}

	function hook_article_filter($article) {

		$enabled_feeds = $this->host->get($this, "enabled_feeds");
		if (!is_array($enabled_feeds)) return $article;

		$key = array_search($article["feed"]["id"], $enabled_feeds);
		if ($key === FALSE) return $article;

		$article["language"] = "he";
		return $article;
	}

	function hook_prefs_edit_feed($feed_id) {
		// $this->log->log("feed_id: ". $feed_id);

		print "<header>".__("Rtl_Support")."</header>";
		print "<section>";

		$enabled_feeds = $this->host->get($this, "enabled_feeds");
		if (!is_array($enabled_feeds)) $enabled_feeds = array();

		$key = array_search($feed_id, $enabled_feeds);
		$checked = $key !== FALSE ? "checked" : "";

		print "<fieldset>";

		print "<label class='checkbox'><input dojoType='dijit.form.CheckBox' type='checkbox' id='rtl_support_enabled'
			name='rtl_support_enabled' $checked>&nbsp;".__('Right to Left article')."</label>";

		print "</fieldset>";

		print "</section>";
	}

	function hook_prefs_save_feed($feed_id) {
		$enabled_feeds = $this->host->get($this, "enabled_feeds");
		// $this->log->log("enabled_feeds: " . implode(",", $enabled_feeds));
		if (!is_array($enabled_feeds)) $enabled_feeds = array();

		$enable = checkbox_to_sql_bool($_POST["rtl_support_enabled"]);
		$key = array_search($feed_id, $enabled_feeds);

		if ($enable) {
			if ($key === FALSE) {
				array_push($enabled_feeds, $feed_id);
			}
		} else {
			if ($key !== FALSE) {
				unset($enabled_feeds[$key]);
			}
		}

		// $this->log->log("enabled_feeds: " . implode(",", $enabled_feeds));
		$this->host->set($this, "enabled_feeds", $enabled_feeds);
	}

	function api_version() {
		return 2;
	}

}
?>
